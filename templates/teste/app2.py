from flask import Flask, render_template

app = Flask(__name__)

@app.route("/")
def index():
    return render_template("teste3.html")

@app.route("/pessoas")
def get_pessoas():
    usuarios = [
        {"nome":"daniel"},
        {"nome":"pessoa2"},
        {"nome":"pessoa3"},
        {"nome":"pessoa4"},
        {"nome":"pessoa5"},
        {"nome":"pessoa6"},
        {"nome":"pessoa7"}
    ]
    return render_template("teste2.html", context=usuarios)

if __name__ == "__main__":
    app.run(debug=True)