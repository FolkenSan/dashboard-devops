from flask import Blueprint, render_template
from docker import DockerClient
import requests


docker = Blueprint("docker_routes",__name__, url_prefix="/docker")
con = DockerClient("tcp://127.0.0.1:2376")


@docker.route("")
def get_containers():
    container = con.containers.list(all=True)
    # container = con.containers.get(all=True)
    # print(dir(container))
    return render_template("docker.html", containers=container)